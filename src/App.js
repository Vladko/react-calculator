import React, { Component } from "react";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import StandardCalculator from "./componets/calculators/StandardCalculator";
import LoanCalculator from "./componets/calculators/LoanCalculator";
import MortgageCalculator from "./componets/calculators/MortgageCalculator";
import BmiCalculator from "./componets/calculators/BmiCalculators/BmiCalculator";
import SideNav from "./componets/SideNav";
import CurrencyConverter from "./componets/calculators/CurrencyConverter";
import BmrCalculator from "./componets/calculators/BmrCalculators/BmrCalculator";
import IdealWeightCalculator from "./componets/calculators/IdealWeightCalculator";
import AgeCalculator from "./componets/calculators/AgeCalculator";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="container-fluid">
          <SideNav />
          <div className="row">
            <div className="col-lg-9 co-l-md-12 col-sm-12 offset-lg-3 ">
              <Switch>
                <Route exact path="/" component={StandardCalculator} />
                <Route path="/loanCalculator" component={LoanCalculator} />
                <Route path="/mortgageCalculator" component={MortgageCalculator} />
                <Route path="/bmiCalculator" component={BmiCalculator} />
                <Route path="/currencyConverter" component={CurrencyConverter} />
                <Route path="/bmrCalculator" component={BmrCalculator} />
                <Route path="/idealWeightCalculator" component={IdealWeightCalculator} />
                <Route path="/ageCalculator" component={AgeCalculator} />
              </Switch>
            </div>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
