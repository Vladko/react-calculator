import React, { Component } from "react";

class Header extends Component {
  render() {
    return (
      <nav className="navbar sticky-top  navbar-dark bg-primary justify-content-center">
        <a className="navbar-brand" href="/">
          Calculators
        </a>
        <button id="mobile-nav-toggle" onClick={this.props.toggleClick} type="button">
          {this.props.isExpanded === false ? <i className="fa fa-bars"></i> : <i className="fa fa-times"></i>}
        </button>
      </nav>
    );
  }
}

export default Header;
