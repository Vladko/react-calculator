import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import Header from "./Header";
import MobileNAv from "./MobileNav";
class SideNav extends Component {
  constructor() {
    super();
    this.state = {
      isExpanded: false
    };
  }

  toggleClick = () => {
    this.setState({
      isExpanded: !this.state.isExpanded
    });
  };
  render() {
    const { isExpanded } = this.state;
    return (
      <div>
        <Header toggleClick={this.toggleClick} isExpanded={isExpanded} />
        <div className="sidenav bg-primary">
          <ul>
            <li className="font-weight-bold text-white">Financial Calculators</li>
            <li>
              <NavLink exact to="/" activeClassName="active">
                Standard calculator
              </NavLink>
            </li>
            <li>
              <NavLink to="/loanCalculator" activeClassName="active">
                Loan Calculator
              </NavLink>
            </li>
            <li>
              <NavLink to="/mortgageCalculator" activeClassName="active">
                Mortgage Calculator
              </NavLink>
            </li>
            <li>
              <NavLink to="/CurrencyConverter" activeClassName="active">
                Currency Converter
              </NavLink>
            </li>
            <li className="font-weight-bold text-white">Fitness and Health Calculators</li>
            <li>
              <NavLink to="/bmiCalculator" activeClassName="active">
                BMI Calculator
              </NavLink>
            </li>
            <li>
              <NavLink to="/bmrCalculator" activeClassName="active">
                BMR Calculator
              </NavLink>
            </li>
            <li>
              <NavLink to="/idealWeightCalculator" activeClassName="active">
                Ideal Weight Calculator
              </NavLink>
            </li>
            <li className="font-weight-bold text-white">Other Calculators</li>
            <li>
              <NavLink to="/ageCalculator" activeClassName="active">
                Age Calculator
              </NavLink>
            </li>
          </ul>
        </div>
        {isExpanded && <MobileNAv toggleClick={this.toggleClick} />}
      </div>
    );
  }
}

export default SideNav;
