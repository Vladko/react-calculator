import React, { Component } from "react";
import { NavLink } from "react-router-dom";
class MobileNav extends Component {
  render() {
    return (
      <div className="mobileNav bg-primary">
        <ul>
          <li className="font-weight-bold text-white">Financial Calculators</li>
          <li>
            <NavLink exact to="/" activeClassName="active" onClick={this.props.toggleClick}>
              Standard calculator
            </NavLink>
          </li>
          <li>
            <NavLink to="/loanCalculator" activeClassName="active" onClick={this.props.toggleClick}>
              Loan Calculator
            </NavLink>
          </li>
          <li>
            <NavLink to="/mortgageCalculator" activeClassName="active" onClick={this.props.toggleClick}>
              Mortgage Calculator
            </NavLink>
          </li>
          <li>
            <NavLink to="/CurrencyConverter" activeClassName="active" onClick={this.props.toggleClick}>
              Currency Converter
            </NavLink>
          </li>
          <li className="font-weight-bold text-white">Fitness and Health Calculators</li>
          <li>
            <NavLink to="/bmiCalculator" activeClassName="active" onClick={this.props.toggleClick}>
              BMI Calculator
            </NavLink>
          </li>
          <li>
            <NavLink to="/bmrCalculator" activeClassName="active" onClick={this.props.toggleClick}>
              BMR Calculator
            </NavLink>
          </li>
          <li>
            <NavLink to="/idealWeightCalculator" activeClassName="active" onClick={this.props.toggleClick}>
              Ideal Weight Calculator
            </NavLink>
          </li>
          <li className="font-weight-bold text-white">Other Calculators</li>
          <li>
            <NavLink to="/ageCalculator" activeClassName="active" onClick={this.props.toggleClick}>
              Age Calculator
            </NavLink>
          </li>
        </ul>
      </div>
    );
  }
}

export default MobileNav;
