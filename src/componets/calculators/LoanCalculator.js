import React, { Component } from "react";
import messages from "../Messages";

class LoanCalculator extends Component {
  constructor() {
    super();
    this.state = {
      amount: "",
      term: "",
      rate: "",
      payments: "",
      totalRate: "",
      individualPay: "",
      validateMessage: false
    };
  }
  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  loanCalculator = () => {
    const { amount, term, rate } = this.state;
    const interest = rate * amount;
    const interestRate = interest / 100;
    const total = Number(amount) + Number(interestRate);
    const payment = total / term;
    return this.setState({
      totalRate: interestRate.toFixed(2),
      payments: total.toFixed(2),
      individualPay: payment.toFixed(2),
      validateMessage: false
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { amount, term, rate } = this.state;
    if (amount === "" || term === "" || rate === "") {
      return this.setState(
        {
          validateMessage: true
        },
        () => {
          setTimeout(() => {
            this.setState({
              validateMessage: false
            });
          }, 3000);
        }
      );
    } else {
      this.loanCalculator();
    }
  };
  render() {
    const { amount, term, rate, payments, totalRate, individualPay, validateMessage } = this.state;

    return (
      <div className="container mt-5">
        <h1 className="text-center m-4 font-weight-bold">{messages.loanCalculator.name}</h1>
        <hr className="mb-5" />
        <div className="row">
          <div className="col-lg-6 col-sm-12">
            <form onSubmit={this.onSubmit}>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">{messages.labels.amount}</span>
                </div>
                <input type="number" className="form-control" name="amount" value={amount} onChange={this.onChange} placeholder="How much money" />
              </div>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">{messages.labels.term}</span>
                </div>
                <input type="number" className="form-control" name="term" value={term} onChange={this.onChange} placeholder="Months" />
              </div>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">{messages.labels.rate}</span>
                </div>
                <input type="number" className="form-control" name="rate" value={rate} step="0.01" onChange={this.onChange} placeholder="%" />
                <div className="input-group-append">
                  <span className="input-group-text">%</span>
                </div>
              </div>

              {validateMessage === true ? (
                <div className="alert alert-warning text-center" role="alert">
                  Please fill all fields
                </div>
              ) : null}

              <div className="my-4 text-center">
                <button type="submit" className="btn btn-info ">
                  {messages.labels.calculate}
                </button>
              </div>
            </form>
          </div>
          <div className="col-lg-4 col-sm-12">
            <ul className="list-group">
              <li className="list-group-item">
                {messages.loanCalculator.paymentText[0]} {individualPay}
              </li>
              <li className="list-group-item">
                {messages.loanCalculator.paymentText[1]} {payments}
              </li>
              <li className="list-group-item">
                {messages.loanCalculator.paymentText[2]} {totalRate}
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default LoanCalculator;
