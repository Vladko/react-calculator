import React, { Component } from "react";
import messages from "../Messages";

class CurrencyConverter extends Component {
  state = {
    currencies: messages.currencyConverter.currencies,
    base: "USD",
    amount: "",
    convertTo: "EUR",
    result: "",
    date: ""
  };

  handleSelect = e => {
    this.setState(
      {
        [e.target.name]: e.target.value,
        result: ""
      },
      this.calculate
    );
  };

  handleInput = e => {
    this.setState(
      {
        amount: e.target.value,
        result: "",
        date: ""
      },
      this.calculate
    );
  };

  calculate = () => {
    const amount = this.state.amount;
    if (amount === isNaN) {
      return;
    } else {
      fetch(`https://api.exchangeratesapi.io/latest?base=${this.state.base}`)
        .then(res => res.json())
        .then(data => {
          console.log(data);
          const date = data.date;
          const result = (data.rates[this.state.convertTo] * amount).toFixed(4);
          this.setState({
            result: result,
            date: date
          });
        });
    }
  };

  handleSwap = e => {
    const base = this.state.base;
    const convertTo = this.state.convertTo;
    e.preventDefault();
    this.setState(
      {
        convertTo: base,
        base: convertTo,
        result: null
      },
      this.calculate
    );
  };
  render() {
    const { currencies, base, amount, convertTo, result, date } = this.state;
    return (
      <div className="container my-5">
        <div className="row">
          <div className="col-xs-12   col-sm-12   col-md-12 col-lg-6">
            <div className="card  ">
              <div className="small ">
                <h2 className="font-weight-bold text-center">{messages.currencyConverter.name} </h2>
              </div>
              <p className="font-weight-bold">
                {messages.currencyConverter.date} : {date}
              </p>
              <form className="form-inline  mb-4">
                <input value={amount} type="number" onChange={this.handleInput} className="form-control bg-light  my-2 mr-2" placeholder="Amount" />
                <select name="base" value={base} onChange={this.handleSelect} className="form-control">
                  {currencies.map(currency => (
                    <option key={currency} value={currency}>
                      {currency}
                    </option>
                  ))}
                </select>
              </form>

              <form className="form-inline mb-3">
                <input disabled={true} value={result} className="form-control   text-dark my-2 mr-2" />
                <select name="convertTo" value={convertTo} onChange={this.handleSelect} className="form-control ">
                  {currencies.map(currency => (
                    <option key={currency} value={currency}>
                      {currency}
                    </option>
                  ))}
                </select>
              </form>

              <div className="col-lg-4 align-self-center ">
                <h1 onClick={this.handleSwap} className="swap text-center">
                  &#8595;&#8593;
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CurrencyConverter;
