import React, { Component } from "react";

class StandardCalculator extends Component {
  constructor() {
    super();

    this.state = {
      result: ""
    };
  }

  onClick = value => {
    if (value === "=") {
      this.calculate();
    } else if (value === "C") {
      this.reset();
    } else if (value === "CE") {
      this.backspace();
    } else {
      this.setState({
        result: this.state.result + value
      });
    }
  };

  calculate = () => {
    var checkResult = "";
    if (this.state.result.includes("--")) {
      checkResult = this.state.result.replace("--", "+");
    } else {
      checkResult = this.state.result;
    }

    try {
      this.setState({
        // eslint-disable-next-line
        result: (eval(checkResult) || "") + ""
      });
    } catch (e) {
      this.setState({
        result: "Invalid input"
      });
    }
  };

  reset = () => {
    this.setState({
      result: ""
    });
  };

  backspace = () => {
    this.setState({
      result: this.state.result.slice(0, -1)
    });
  };
  render() {
    const { result } = this.state;
    return (
      <div className="container " id="CalculatorBasic">
        <div className="row  ">
          <div className=" col-xs-12   col-sm-12   col-md-12  col-lg-9 ">
            <div className=" bg-primary border mt-5 p-4">
              <form name="form" className=" text-center ">
                <p className=" p-2" id="panel" name="panel" placeholder="0." disabled>
                  {result}
                </p>
                <br />

                <input className="form-group btn btn-secondary bttn " type="button" name="bttn7" value="7" onClick={e => this.onClick(e.target.value)} />
                <input className="form-group btn btn-secondary bttn" type="button" name="bttn8" value="8" onClick={e => this.onClick(e.target.value)} />
                <input className="form-group btn btn-secondary bttn" type="button" name="bttn9" value="9" onClick={e => this.onClick(e.target.value)} />
                <input className="form-group btn btn-danger bttn" type="button" name="bttnplus" value="+" onClick={e => this.onClick(e.target.value)} />
                <br />
                <input className="form-group btn btn-secondary bttn" type="button" name="bttn4" value="4" onClick={e => this.onClick(e.target.value)} />
                <input className="form-group btn btn-secondary bttn" type="button" name="bttn5" value="5" onClick={e => this.onClick(e.target.value)} />
                <input className="form-group btn btn-secondary bttn" type="button" name="bttn6" value="6" onClick={e => this.onClick(e.target.value)} />
                <input className="form-group btn btn-danger bttn " type="button" name="bttnminus" value="-" onClick={e => this.onClick(e.target.value)} />
                <br />

                <input className="form-group btn btn-secondary bttn" type="button" name="bttn1" value="1" onClick={e => this.onClick(e.target.value)} />
                <input className="form-group btn btn-secondary bttn" type="button" name="bttn2" value="2" onClick={e => this.onClick(e.target.value)} />
                <input className="form-group btn btn-secondary bttn" type="button" name="bttn3" value="3" onClick={e => this.onClick(e.target.value)} />
                <input className="form-group btn btn-danger bttn " type="button" name="bttnmulti" value="*" onClick={e => this.onClick(e.target.value)} />
                <br />
                <input className="form-group btn btn-secondary bttn" type="button" name="bttndot" value="." onClick={e => this.onClick(e.target.value)} />
                <input className="form-group btn btn-secondary bttn" type="button" name="bttn0" value="0" onClick={e => this.onClick(e.target.value)} />
                <input className="form-group btn btn-danger bttn " type="button" name="bttnmod" value="%" onClick={e => this.onClick(e.target.value)} />
                <input className="form-group btn btn-danger bttn" type="button" name="bttndiv" value="/" onClick={e => this.onClick(e.target.value)} />
                <br />

                <input className="form-group btn btn-info  bttne" type="button" name="bttnclear" value="CE" onClick={e => this.onClick(e.target.value)} />
                <input className="form-group btn btn-info  bttne" type="button" name="bttnclear" value="C" onClick={e => this.onClick(e.target.value)} />
                <input className="form-group btn btn-success  bttne" type="button" name="bttnEQL" value="=" onClick={e => this.onClick(e.target.value)} />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default StandardCalculator;
