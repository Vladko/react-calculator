import React, { Component } from "react";
import messages from "../Messages";
class IdealWeightCalculator extends Component {
  constructor() {
    super();
    this.state = {
      age: "",
      gender: "",
      height: "",
      result: "",
      validateMessage: ""
    };
  }

  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  radioChange = e => {
    this.setState({
      gender: e.currentTarget.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { gender, height } = this.state;
    const resultMen = Number(height) - 105;
    const resultWomen = Number(height) - 110;

    if (gender === "Male") {
      this.setState({
        result: messages.idealWeightCalculator.result + resultMen + " kg"
      });
    } else if (gender === "Female") {
      this.setState({
        result: messages.idealWeightCalculator.result + resultWomen + " kg"
      });
    }
  };

  render() {
    const { result } = this.state;
    return (
      <div className="container mt-5">
        <h3 className="text-center m-4 font-weight-bold">{messages.idealWeightCalculator.name}</h3>

        <p>
          The Ideal Weight Calculator computes ideal bodyweight (IBW) ranges based on height, gender, and age. The idea of finding the IBW using a formula has been sought after by many experts for a long time. Currently, there persist several popular formulas, and our Ideal Weight Calculator
          provides their results for side-to-side comparisons.
        </p>
        <hr />
        <form onSubmit={this.onSubmit}>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text">{messages.labels.age}</span>
            </div>
            <p>{this.state.validateMessage}</p>
            <input type="number" className="form-control" name="age" onChange={this.onChange} placeholder="ages: 2 - 120" />
          </div>
          <div className="form-check form-check-inline mb-3">
            <span className="input-group-text mr-2">{messages.labels.gender}</span>
            <input type="radio" value="Male" checked={this.state.gender === "Male"} onChange={this.radioChange} />
            {messages.labels.m}
          </div>
          <div className="form-check form-check-inline ">
            <input type="radio" value="Female" checked={this.state.gender === "Female"} onChange={this.radioChange} />
            {messages.labels.f}
          </div>
          <div className="input-group mb-3 ">
            <div className="input-group-prepend">
              <span className="input-group-text">{messages.labels.height}</span>
            </div>
            <input type="number" className="form-control" name="height" onChange={this.onChange} placeholder="cm" />
            <div className="input-group-append">
              <span className="input-group-text ">CM</span>
            </div>
          </div>
          <div className="text-center font-weight-bold">
            <div className="alert alert-success" role="alert">
              <h2> {result}</h2>
            </div>
          </div>
          <div className="mt-4 text-center">
            <button type="submit" className="btn btn-info btn-block">
              {messages.labels.calculate}
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default IdealWeightCalculator;
