import React, { Component } from "react";
import messages from "../../Messages";
class BmiCalculatorMetric extends Component {
  constructor() {
    super();
    this.state = {
      age: "",
      gender: "",
      height: "",
      weight: "",
      result: "",
      className: "",
      bmiIndex: "",
      validateMessage: false
    };
  }

  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  radioChange = e => {
    this.setState({
      gender: e.currentTarget.value
    });
  };
  calculateBmi = () => {
    const { height, weight } = this.state;
    const heightToMeters = Number(height) / 100;
    const heightToSquare = Number(heightToMeters) * Number(heightToMeters);
    const finalResult = Number(weight) / Number(heightToSquare);
    let message = "";
    let className = "";
    let bmiIndex = null;

    if (finalResult >= 18.5 && finalResult <= 24.99) {
      message = messages.bmi.healthy;
      bmiIndex = finalResult;
      className = "alert alert-success";
    } else if (finalResult >= 25 && finalResult <= 29.9) {
      message = messages.bmi.overweight;
      bmiIndex = finalResult;
      className = "alert alert-danger";
    } else if (finalResult >= 30) {
      message = messages.bmi.obese;
      bmiIndex = finalResult;
      className = "alert alert-warning";
    } else if (finalResult < 18.5) {
      message = messages.bmi.underWeight;
      className = "alert alert-primary";
      bmiIndex = finalResult;
    }
    this.setState({ result: message, className: className, bmiIndex: "Your bmi index is " + bmiIndex.toFixed(1) });
  };
  onSubmit = e => {
    e.preventDefault();
    const { height, weight, age, gender } = this.state;
    if (height === "" || weight === "" || age === "" || gender === "") {
      return this.setState(
        {
          validateMessage: true
        },
        () => {
          setTimeout(() => {
            this.setState({
              validateMessage: false
            });
          }, 3000);
        }
      );
    } else {
      this.calculateBmi();
    }
  };

  render() {
    const { result, className, bmiIndex, validateMessage } = this.state;
    return (
      <div className="container mt-5">
        <div className="row">
          <div className="col-lg-6 col-sm-12">
            <form onSubmit={this.onSubmit}>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">{messages.labels.age}</span>
                </div>
                <input type="number" className="form-control" name="age" onChange={this.onChange} placeholder="ages: 2 - 120" />
              </div>
              <div className="form-check form-check-inline mb-3">
                <span className="input-group-text mr-2">{messages.labels.gender}</span>
                <input type="radio" value="Male" checked={this.state.gender === "Male"} onChange={this.radioChange} />
                {messages.labels.m}
              </div>
              <div className="form-check form-check-inline ">
                <input type="radio" value="Female" checked={this.state.gender === "Female"} onChange={this.radioChange} />
                {messages.labels.f}
              </div>
              <div className="input-group mb-3 ">
                <div className="input-group-prepend">
                  <span className="input-group-text">{messages.labels.height}</span>
                </div>
                <input type="number" className="form-control" name="height" onChange={this.onChange} placeholder="cm" />
                <div className="input-group-append">
                  <span className="input-group-text ">CM</span>
                </div>
              </div>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">{messages.labels.weight}</span>
                </div>
                <input type="number" className="form-control" name="weight" onChange={this.onChange} placeholder="kg" />
                <div className="input-group-append">
                  <span className="input-group-text">KG</span>
                </div>
              </div>
              {validateMessage === true ? (
                <div className="alert alert-warning text-center" role="alert">
                  Please fill all fields
                </div>
              ) : null}
              <div className="mt-4 text-center">
                <button type="submit" className="btn btn-info">
                  {messages.labels.calculate}
                </button>
              </div>
            </form>
          </div>
          <div className="col-lg-4 col-sm-12">
            <div className="text-center font-weight-bold">
              <div className={className} role="alert">
                <p>{bmiIndex}</p>
                <h2> {result}</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BmiCalculatorMetric;
