import React from "react";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import BmiCalculatorMetric from "./BmiCalculatorMetric";
import BmiCalculatorUs from "./BmiCalculatorUs";
import messages from "../../Messages";

function BmiCalculator() {
  return (
    <div>
      <h3 className="text-center mt-3 font-weight-bold">{messages.bmi.name}</h3>
      <p>The body mass index (BMI) is a measure that uses your height and weight to work out if your weight is healthy.</p>
      <hr className="mb-5" />

      <Tabs defaultActiveKey={messages.labels.unitsMetric} transition={false} id="noanim-tab-example" className="m-5">
        <Tab eventKey={messages.labels.unitsMetric} title={messages.labels.unitsMetric}>
          <BmiCalculatorMetric />
        </Tab>
        <Tab eventKey={messages.labels.unitsUs} title={messages.labels.unitsUs}>
          <BmiCalculatorUs />
        </Tab>
      </Tabs>
    </div>
  );
}

export default BmiCalculator;
