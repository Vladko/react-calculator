import React, { Component } from "react";
import messages from "../Messages";

class AgeCalculator extends Component {
  constructor() {
    super();
    this.state = {
      days: "",
      year: "",
      months: "",
      finalResult: "",
      result: false
    };
  }

  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSelect = e => {
    this.setState({
      [e.target.name]: e.currentTarget.value
    });
  };

  calculate = () => {
    const { days, year, months } = this.state;
    const date = new Date();
    const date2 = new Date(year, months, days);
    const age = date.getFullYear() - date2.getFullYear();
    let ages = age;
    let month = date.getMonth() - date2.getMonth() - 1;
    let resultdays = "";

    if (date.getMonth() === date2.getMonth()) {
      if (date.getDate() > date2.getDate()) {
        resultdays = date.getDate() - date2.getDate();
      } else {
        resultdays = date.getDate();
      }

      this.setState({
        finalResult: ` You are   ${ages}  years  and 0   months  and  ${resultdays} days old`
      });
    } else if (date.getMonth() < date2.getMonth()) {
      ages = age - 1;
      month = date.getMonth();
      this.setState({
        finalResult: ` You are   ${ages}  years  and  ${month}  months  old `
      });
    } else {
      this.setState({
        finalResult: ` You are   ${ages}  years  and ${month}   months  old `
      });
    }
  };

  onSubmit = e => {
    const { days, year, months } = this.state;
    e.preventDefault();
    if (days === "" || year === "" || months === "") {
      this.setState(
        {
          finalResult: "Please fill all fields",
          result: true
        },
        () => {
          setTimeout(() => {
            this.setState({
              result: false
            });
          }, 3000);
        }
      );
    } else {
      this.setState({
        result: true
      });
      this.calculate();
    }
  };
  render() {
    const { finalResult, result } = this.state;
    return (
      <div>
        <div className="container ">
          <h3 className="text-center mb-2 mt-5 font-weight-bold">{messages.ageCalculator.name}</h3>
          <p>The Age Calculator can determine the age or interval between two dates. The calculated age will be displayed in years and months</p>
          <hr className="mb-5" />
          <form onSubmit={this.onSubmit}>
            <h3>Date of Birth :</h3>
            <div className="row">
              <div className="col-lg-4">
                <div className="input-group mt-3">
                  <div className="input-group-prepend">
                    <label className="input-group-text" htmlFor="inputGroupSelect01">
                      Month
                    </label>
                  </div>
                  <select className="custom-select" name="months" id="inputGroupSelect01" onChange={this.onSelect}>
                    <option value="">Choose...</option>
                    {messages.ageCalculator.months.map((month, id) => {
                      return (
                        <option key={id} value={messages.ageCalculator.months.indexOf(month)}>
                          {month}
                        </option>
                      );
                    })}
                  </select>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="input-group mt-3">
                  <div className="input-group-prepend">
                    <label className="input-group-text" htmlFor="inputGroupSelect01">
                      Day
                    </label>
                  </div>
                  <select className="custom-select" name="days" id="inputGroupSelect01" onChange={this.onSelect}>
                    <option value="">Choose...</option>
                    {messages.ageCalculator.days.map((day, id) => {
                      return (
                        <option key={id} value={day}>
                          {day}
                        </option>
                      );
                    })}
                  </select>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="input-group mt-3">
                  <div className="input-group-prepend">
                    <label className="input-group-text" htmlFor="inputGroupSelect01">
                      Year
                    </label>
                  </div>
                  <input type="number" className="form-control" name="year" placeholder="Year" onChange={this.onChange} />
                </div>
              </div>
            </div>

            {result === true ? (
              <div className="text-center font-weight-bold mt-3">
                <div className="alert alert-success" role="alert">
                  <h2>{finalResult}</h2>
                </div>
              </div>
            ) : null}
            <div className="mt-4 text-center">
              <button type="submit" className="btn btn-info btn-block">
                {messages.labels.calculate}
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default AgeCalculator;
