import React, { Component } from "react";
import messages from "../Messages";

export class MortgageCalculator extends Component {
  constructor() {
    super();
    this.state = {
      price: "",
      term: "",
      rate: "",
      downPayment: "",
      totalRate: "",
      monthlyPay: "",
      loanAmount: "",
      totalMonths: "",
      totalPayment: "",
      validateMessage: false
    };
  }
  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
    console.log(this.state);
  };
  mortgageCalculator = () => {
    const { price, term, rate, downPayment } = this.state;
    const loanAmounts = price - downPayment;
    const totalMonths = term * 12;
    const interest = rate * loanAmounts;
    const interestRate = Number(interest) / 100;
    const monthlyPays = (Number(loanAmounts) + Number(interestRate)) / totalMonths;
    const total = Number(loanAmounts) + Number(interestRate);
    return this.setState({
      totalRate: interestRate,
      monthlyPay: monthlyPays.toFixed(2),
      loanAmount: loanAmounts.toFixed(2),
      totalMonths: totalMonths,
      totalPayment: total
    });
  };
  onSubmit = e => {
    e.preventDefault();
    const { price, term, rate, downPayment } = this.state;
    if (price === "" || term === "" || rate === "" || downPayment === "") {
      return this.setState(
        {
          validateMessage: true
        },
        () => {
          setTimeout(() => {
            this.setState({
              validateMessage: false
            });
          }, 3000);
        }
      );
    } else {
      this.mortgageCalculator();
    }
  };
  render() {
    const { totalMonths, totalPayment, totalRate, monthlyPay, loanAmount, validateMessage } = this.state;
    return (
      <div className="container mt-5">
        <h1 className="text-center m-4 font-weight-bold">{messages.mortgageCalculator.name}</h1>
        <hr className="mb-5" />
        <div className="row">
          <div className="col-lg-6 col-sm-12">
            <form onSubmit={this.onSubmit}>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">{messages.labels.price}</span>
                </div>
                <input type="number" className="form-control" name="price" onChange={this.onChange} placeholder="Price" />
              </div>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text pr-5">{messages.labels.down}</span>
                </div>
                <input type="number" className="form-control" name="downPayment" onChange={this.onChange} placeholder="Money in advance" />
              </div>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text pr-5">{messages.labels.down}</span>
                </div>
                <input type="number" className="form-control" name="term" onChange={this.onChange} placeholder="How many Years" />
                <div className="input-group-append">
                  <span className="input-group-text">{messages.labels.years}</span>
                </div>
              </div>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">{messages.labels.rate}</span>
                </div>
                <input type="number" className="form-control" name="rate" step="0.01" onChange={this.onChange} placeholder="%" />
                <div className="input-group-append">
                  <span className="input-group-text">%</span>
                </div>
              </div>
              {validateMessage === true ? (
                <div className="alert alert-warning text-center" role="alert">
                  Please fill all fields
                </div>
              ) : null}
              <div className="my-4 text-center">
                <button type="submit" className="btn btn-info">
                  {messages.labels.calculate}
                </button>
              </div>
            </form>
          </div>
          <div className="col-lg-4 col-sm-12">
            <ul className="list-group">
              <li className="list-group-item">
                {messages.mortgageCalculator.paymentText[0]} {loanAmount}
              </li>
              <li className="list-group-item">
                {messages.mortgageCalculator.paymentText[1]} {monthlyPay}
              </li>
              <li className="list-group-item">
                Total of {totalMonths} {messages.mortgageCalculator.paymentText[2]} {totalPayment}
              </li>
              <li className="list-group-item">
                {messages.mortgageCalculator.paymentText[3]} {totalRate}
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default MortgageCalculator;
