import React, { Component } from "react";
import messages from "../Messages";
class WorkTimeCalculator extends Component {
  constructor() {
    super();
    this.state = {
      mondayStart: "",
      mondayEnd: "",
      breakTimeMonday: "",
      tuesdayStart: "",
      tuesdayEnd: "",
      breakTimeTuesday: "",
      wednesdayStart: "",
      wednesdayEnd: "",
      breakTimeWednesday: "",
      thursdayStart: "",
      thursdayEnd: "",
      breakTimeThursday: "",
      fridayStart: "",
      fridayEnd: "",
      breakTimeFriday: "",
      saturdayStart: "",
      saturdayEnd: "",
      breakTimeSaturday: "",
      sundayStart: "",
      sundayEnd: "",
      breakTimeSunday: "",
      monday: "",
      tuesday: "",
      wednesday: "",
      thursday: "",
      friday: "",
      saturday: "",
      sunday: ""
    };
  }

  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  calculateTime = (start, end, breakTime) => {
    let start1 = start.split(".");
    let end1 = end.split(".");
    let startDate = new Date(0, 0, 0, start1[0], start1[1], 0);
    let endDate = new Date(0, 0, 0, end1[0], end1[1], 0);
    let hoursWork = endDate.getTime() - startDate.getTime();
    let hours = Math.floor(hoursWork / 1000 / 60 / 60);
    hoursWork -= hours * 1000 * 60 * 60;
    let minutes = Math.floor(hoursWork / 1000 / 60);
    hoursWork = ` ${hours} h  ${minutes} m`;
    let finalDate = new Date(0, 0, 0, hours, minutes, 0, 0);
    finalDate.setMinutes(finalDate.getMinutes() - breakTime);
    if (start === "" && end === "" && breakTime === "") {
      return "";
    } else {
      return ` ${finalDate.getHours()} h  ${finalDate.getMinutes()} m`;
    }
  };
  getMonday = () => {
    this.setState({
      monday: this.calculateTime(this.state.mondayStart, this.state.mondayEnd, this.state.breakTimeMonday)
    });
  };
  getTuesday = () => {
    this.setState({
      tuesday: this.calculateTime(this.state.tuesdayStart, this.state.tuesdayEnd, this.state.breakTimeTuesday)
    });
  };
  getWendesday = () => {
    this.setState({
      wednesday: this.calculateTime(this.state.wednesdayStart, this.state.wednesdayEnd, this.state.breakTimeWednesday)
    });
  };
  getThursday = () => {
    this.setState({
      thursday: this.calculateTime(this.state.thursdayStart, this.state.thursdayEnd, this.state.breakTimeThursday)
    });
  };
  getFriday = () => {
    this.setState({
      friday: this.calculateTime(this.state.fridayStart, this.state.fridayEnd, this.state.breakTimeFriday)
    });
  };

  getSaturday = () => {
    this.setState({
      saturday: this.calculateTime(this.state.saturdayStart, this.state.saturdayEnd, this.state.breakTimeSaturday)
    });
  };

  getSunday = () => {
    this.setState({
      sunday: this.calculateTime(this.state.sundayStart, this.state.sundayEnd, this.state.breakTimeSunday)
    });
  };

  onSubmit = e => {
    e.preventDefault();
    this.getMonday();
    this.getTuesday();
    this.getWendesday();
    this.getThursday();
    this.getFriday();
    this.getSaturday();
    this.getSunday();
  };

  render() {
    console.log(this.state);
    return (
      <div className="col-lg-8 col-sm-12">
        <h1 className="text-center m-4 font-weight-bold">{messages.workTime.name}</h1>
        <hr className="mb-3" />
        <form onSubmit={this.onSubmit}>
          <table className="table">
            <thead>
              <tr>
                <th scope="col">{messages.workTime.tableTitle[0]}</th>
                <th scope="col">{messages.workTime.tableTitle[1]}</th>
                <th scope="col">{messages.workTime.tableTitle[2]}</th>
                <th scope="col">{messages.workTime.tableTitle[3]}</th>
                <th scope="col">{messages.workTime.tableTitle[4]}</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">{messages.workTime.tableSubtitle[0]}</th>
                <td>
                  <input type="number" className="form-control mobile" step="00.01" name="mondayStart" placeholder="H.M" onChange={this.onChange} />
                </td>
                <td>
                  <input type="number" className="form-control mobile" step="00.01" name="mondayEnd" placeholder="H.M" onChange={this.onChange} />
                </td>
                <td>
                  <input type="number" className="form-control mobile" step="00.01" name="breakTimeMonday" placeholder="m" onChange={this.onChange} />
                </td>
                <td>
                  <input type="text" className="form-control mobile" value={this.state.monday} readOnly />
                </td>
              </tr>
              <tr>
                <th scope="row">{messages.workTime.tableSubtitle[1]}</th>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="tuesdayStart" onChange={this.onChange} placeholder="H.M" />
                </td>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="tuesdayEnd" onChange={this.onChange} placeholder="H.M" />
                </td>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="breakTimeTuesday" placeholder="m" onChange={this.onChange} />
                </td>
                <td>
                  <input type="text" className="form-control mobile" value={this.state.tuesday} readOnly />
                </td>
              </tr>
              <tr>
                <th scope="row">{messages.workTime.tableSubtitle[2]}</th>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="wednesdayStart" placeholder="H.M" onChange={this.onChange} />
                </td>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="wednesdayEnd" placeholder="H.M" onChange={this.onChange} />
                </td>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="breakTimeWednesday" placeholder="m" onChange={this.onChange} />
                </td>
                <td>
                  <input type="text" className="form-control mobile" value={this.state.wednesday} readOnly />
                </td>
              </tr>
              <tr>
                <th scope="row">{messages.workTime.tableSubtitle[3]}</th>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="thursdayStart" placeholder="H.M" onChange={this.onChange} />
                </td>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="thursdayEnd" placeholder="H.M" onChange={this.onChange} />
                </td>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="breakTimeThursday" placeholder="m" onChange={this.onChange} />
                </td>
                <td>
                  <input type="text" className="form-control mobile" step="0.01" value={this.state.thursday} readOnly />
                </td>
              </tr>
              <tr>
                <th scope="row">{messages.workTime.tableSubtitle[4]}</th>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="fridayStart" placeholder="H.M" onChange={this.onChange} />
                </td>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="fridayEnd" placeholder="H.M" onChange={this.onChange} />
                </td>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="breakTimeFriday" placeholder="m" onChange={this.onChange} />
                </td>
                <td>
                  <input type="text" className="form-control mobile" step="0.01" value={this.state.friday} readOnly />
                </td>
              </tr>
              <tr>
                <th scope="row">{messages.workTime.tableSubtitle[5]}</th>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="saturdayStart" placeholder="H.M" onChange={this.onChange} />
                </td>
                <td>
                  <input type="number" className="form-control mobile" name="saturdayEnd" placeholder="H.M" onChange={this.onChange} />
                </td>
                <td>
                  <input type="number" className="form-control mobile" name="breakTimeSaturday" placeholder="m" onChange={this.onChange} />
                </td>
                <td>
                  <input type="text" className="form-control mobile" value={this.state.saturday} readOnly />
                </td>
              </tr>
              <tr>
                <th scope="row">{messages.workTime.tableSubtitle[6]}</th>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="sundayStart" placeholder="H.M" onChange={this.onChange} />
                </td>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="sundayEnd" placeholder="H.M" onChange={this.onChange} />
                </td>
                <td>
                  <input type="number" className="form-control mobile" step="0.01" name="breakTimeSunday" placeholder="m" onChange={this.onChange} />
                </td>
                <td>
                  <input type="text" className="form-control mobile" value={this.state.sunday} readOnly />
                </td>
              </tr>
            </tbody>
          </table>
          <div className="mt-4 text-center">
            <button type="submit" className="btn btn-info btn-block">
              {messages.labels.calculate}
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default WorkTimeCalculator;
