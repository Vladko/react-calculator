import React from "react";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import BmrCalculatorMetric from "./BmrCalculatorMetric";
import BmrCalculatorUs from "./BmrCalculatorUs";
import messages from "../../Messages";

function BmrCalculator() {
  return (
    <div>
      <h3 className="text-center mt-4 font-weight-bold">BMR Calculator</h3>
      <p>The Basal Metabolic Rate (BMR) Calculator estimates your basal metabolic rate—the amount of energy expended while at rest in a neutrally temperate environment, and in a post-absorptive state (meaning that the digestive system is inactive, which requires about 12 hours of fasting)</p>
      <hr />
      <Tabs defaultActiveKey={messages.labels.unitsMetric} transition={false} id="noanim-tab-example" className="m-5">
        <Tab eventKey={messages.labels.unitsMetric} title={messages.labels.unitsMetric}>
          <BmrCalculatorMetric />
        </Tab>
        <Tab eventKey={messages.labels.unitsUs} title={messages.labels.unitsUs}>
          <BmrCalculatorUs />
        </Tab>
      </Tabs>
    </div>
  );
}

export default BmrCalculator;
