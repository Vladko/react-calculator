import React, { Component } from "react";
import messages from "../../Messages";

class BmrCalculatorUs extends Component {
  constructor() {
    super();
    this.state = {
      age: "",
      gender: "",
      height: "",
      weight: "",
      selectBox: "1",
      calories: "",
      bmrIndex: "",
      validateMessage: false,
      result: false
    };
  }

  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  radioChange = e => {
    this.setState({
      gender: e.currentTarget.value
    });
  };

  onSelect = e => {
    this.setState({
      selectBox: e.currentTarget.value
    });
  };

  calculateBmr = () => {
    const { height, weight, age, gender, selectBox } = this.state;
    const bmrMen = 4.536 * Number(weight) + 15.88 * Number(height) - 5 * Number(age) + 5;
    const bmrWomen = 4.536 * Number(weight) + 4.536 * Number(height) - 5 * Number(age) - 161;

    if (gender === "Male") {
      if (selectBox === "1") {
        const calories = Number(bmrMen) * 1.2;
        this.setState({
          calories: messages.bmr.calories + calories.toFixed(0) + messages.bmr.day,
          bmrIndex: messages.bmr.index + bmrMen.toFixed(0) + messages.bmr.day
        });
      } else if (selectBox === "2") {
        const calories = Number(bmrMen) * 1.375;
        this.setState({
          calories: messages.bmr.calories + calories.toFixed(0) + messages.bmr.day,
          bmrIndex: messages.bmr.index + bmrMen.toFixed(0) + messages.bmr.day
        });
      } else if (selectBox === "3") {
        const calories = Number(bmrMen) * 1.55;
        this.setState({
          calories: messages.bmr.calories + calories.toFixed(0) + messages.bmr.day,
          bmrIndex: messages.bmr.index + bmrMen.toFixed(0) + messages.bmr.day
        });
      } else if (selectBox === "4") {
        const calories = Number(bmrMen) * 1.725;
        this.setState({
          calories: messages.bmr.calories + calories.toFixed(0) + messages.bmr.day,
          bmrIndex: messages.bmr.index + bmrMen.toFixed(0) + messages.bmr.day
        });
      } else if (selectBox === "5") {
        const calories = Number(bmrMen) * 1.9;
        this.setState({
          calories: messages.bmr.calories + calories.toFixed(0) + messages.bmr.day,
          bmrIndex: messages.bmr.index + bmrMen.toFixed(0) + messages.bmr.day
        });
      }
    } else if (gender === "Female") {
      if (selectBox === "1") {
        const calories = Number(bmrWomen) * 1.2;
        this.setState({
          calories: messages.bmr.calories + calories.toFixed(0) + messages.bmr.day,
          bmrIndex: messages.bmr.index + bmrWomen.toFixed(0) + messages.bmr.day
        });
      } else if (selectBox === "2") {
        const calories = Number(bmrWomen) * 1.375;
        this.setState({
          calories: messages.bmr.calories + calories.toFixed(0) + messages.bmr.day,
          bmrIndex: messages.bmr.index + bmrWomen.toFixed(0) + messages.bmr.day
        });
      } else if (selectBox === "3") {
        const calories = Number(bmrWomen) * 1.55;
        this.setState({
          calories: messages.bmr.calories + calories.toFixed(0) + messages.bmr.day,
          bmrIndex: messages.bmr.index + bmrWomen.toFixed(0) + messages.bmr.day
        });
      } else if (selectBox === "4") {
        const calories = Number(bmrWomen) * 1.725;
        this.setState({
          calories: messages.bmr.calories + calories.toFixed(0) + messages.bmr.day,
          bmrIndex: messages.bmr.index + bmrWomen.toFixed(0) + messages.bmr.day
        });
      } else if (selectBox === "5") {
        const calories = Number(bmrWomen) * 1.9;
        this.setState({
          calories: messages.bmr.calories + calories.toFixed(0) + messages.bmr.day,
          bmrIndex: messages.bmr.index + bmrWomen.toFixed(0) + messages.bmr.day
        });
      }
    }
  };
  onSubmit = e => {
    e.preventDefault();
    const { height, weight, age, gender } = this.state;
    if (height === "" || weight === "" || age === "" || gender === "") {
      return this.setState(
        {
          validateMessage: true
        },
        () => {
          setTimeout(() => {
            this.setState({
              validateMessage: false
            });
          }, 3000);
        }
      );
    } else {
      this.setState({
        result: true
      });
      this.calculateBmr();
    }
  };

  render() {
    const { calories, bmrIndex, validateMessage, result } = this.state;
    return (
      <div className="container mt-5">
        <div className="row">
          <div className="col-lg-6 col-sm-12">
            <form onSubmit={this.onSubmit}>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">{messages.labels.age}</span>
                </div>
                <input type="number" className="form-control" name="age" onChange={this.onChange} placeholder="ages: 2 - 120" />
              </div>
              <div className="form-check form-check-inline mb-3">
                <span className="input-group-text mr-2">{messages.labels.gender}</span>
                <input type="radio" value="Male" checked={this.state.gender === "Male"} onChange={this.radioChange} />
                {messages.labels.m}
              </div>
              <div className="form-check form-check-inline ">
                <input type="radio" value="Female" checked={this.state.gender === "Female"} onChange={this.radioChange} />
                {messages.labels.f}
              </div>
              <div className="input-group mb-3 ">
                <div className="input-group-prepend">
                  <span className="input-group-text">{messages.labels.height}</span>
                </div>
                <input type="number" className="form-control" name="height" onChange={this.onChange} placeholder="Inches" />
                <div className="input-group-append">
                  <span className="input-group-text ">Inches</span>
                </div>
              </div>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">{messages.labels.weight}</span>
                </div>
                <input type="number" className="form-control" name="weight" onChange={this.onChange} placeholder="pounds" />
                <div className="input-group-append">
                  <span className="input-group-text">Pounds</span>
                </div>
              </div>
              <div className="form-group">
                <label htmlFor="sel1">Daily Activity:</label>
                <select className="form-control" id="sel1" onChange={this.onSelect}>
                  {messages.bmr.dailyActivity.map((activity, id) => {
                    return (
                      <option value={id + 1} key={id}>
                        {activity}
                      </option>
                    );
                  })}
                </select>
              </div>
              {validateMessage === true ? (
                <div className="alert alert-warning text-center" role="alert">
                  Please fill all fields
                </div>
              ) : null}

              <div className="my-2 text-center">
                <button type="submit" className="btn btn-info">
                  {messages.labels.calculate}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default BmrCalculatorUs;
