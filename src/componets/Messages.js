const messages = {
  labels: {
    age: "Age",
    gender: "Gender",
    height: "Height",
    weight: "Weight",
    m: "Male",
    f: "Female",
    unitsMetric: "Metric Units",
    unitsUs: "Us Units",
    amount: "Loan amount",
    term: "Loan Term",
    rate: "Interst rate",
    price: "Property Price/ Car Price",
    down: "Down Payment",
    years: "Years",
    calculate: "Calculate"
  },
  bmi: {
    name: "BMI Calculator",
    healthy: "You are in a healthy weight",
    overweight: "You are overweight",
    obese: "You are obese",
    underWeight: "You are under weight"
  },
  bmr: {
    calories: "Your  daily calorie requirement: ",
    day: " Calories/day",
    index: "Your BMR index is ",
    dailyActivity: ["No sport/exercise", "Light activity (sport 1-3 times per week)", "Moderate activity (sport 3-5 times per week)", "High activity (everyday exercise)", "Extreme activity (professional athlete)"]
  },
  ageCalculator: {
    name: "Age Calculator",
    months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "Decembar"],
    days: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
  },
  idealWeightCalculator: {
    name: "Ideal Weight Calculator Metric",
    result: "Your ideal weight is  "
  },
  loanCalculator: {
    name: "Loan Calculator",
    paymentText: ["Monthly Payment: ", "Total Payments: ", "Total Interest Rate: "]
  },
  currencyConverter: {
    name: "Currency Converter",
    date: "Valid date",
    currencies: ["EUR (Euro)", "NZD", "ILS", "RUB", "CAD", "USD", "PHP", "CHF", "ZAR", "AUD", "JPY", "TRY", "HKD", "MYR", "THB", "HRK", "NOK", "IDR", "DKK", "CZK", "HUF", "GBP", "MXN", "KRW", "ISK", "SGD", "BRL", "PLN", "RON", "CNY", "SEK"]
  },
  mortgageCalculator: {
    name: "Mortgage Calculator",
    paymentText: ["Loan Amount: ", "Monthly Payment: ", "Mortgage Payments: ", "Total Interest Rate:"]
  },
  workTime: {
    name: "Work Time Calculator",
    tableTitle: ["Days", "Start at:", "End at:", "Lunch Break:", "Hours Worked"],
    tableSubtitle: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
  }
};

export default messages;
